from celery.decorators import task
from celery.utils.log import get_task_logger
from datetime import datetime
from .models import Detail, Project

logger = get_task_logger(__name__)


@task(name="save_details")
def save_details(pk,phrase):
	from selenium import webdriver
	from selenium.webdriver.common.keys import Keys
	from selenium.webdriver.support.ui import WebDriverWait
	from selenium.webdriver.chrome.options import Options
	from bs4 import BeautifulSoup
	import time

	chrome_options = Options()
	chrome_options.add_argument("--headless")
	chrome_options.add_argument("--window-size=1920x1080")

	# Replace this with whatever topic page you'd like to scrape
	quoraTopicPage = 'https://www.quora.com/topic/' + phrase.replace(' ','-')
	# Put your email and password in these variables, make sure to have the quotation marks around them
	yourEmailAddress = "rbu-ong@innovuze.com"
	yourPassword = "just4me"

	# Set a maximum number of questions to scrape
	numberOfQuestionsToScrape = 100

	# Initialize webdriver
	# driver = webdriver.Chrome()
	driver = webdriver.Chrome(chrome_options=chrome_options)
	driver.get("https://www.quora.com/")
	wait = WebDriverWait(driver, 30)

	# Enter Email address and submit
	form = driver.find_element_by_class_name('regular_login')
	username = form.find_element_by_name('email')
	username.send_keys(yourEmailAddress)

	# Enter Password and submit
	password = form.find_element_by_name('password')
	password.send_keys(yourPassword)
	driver.execute_script('document.querySelectorAll(".regular_login .submit_button")[0].click()')

	time.sleep(10)
	# Manually put in 2FA code here

	#navigate to topic page
	driver.get(quoraTopicPage)

	#get div with all questions
	questionDiv = driver.find_element_by_class_name('layout_3col_center')
	questionHTML = questionDiv.get_attribute("innerHTML")
	driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
	# Allow time to update page
	time.sleep(3)

	#get questions again
	questionDiv = driver.find_element_by_class_name('layout_3col_center')
	newQuestionHTML = questionDiv.get_attribute("innerHTML")

	if newQuestionHTML == questionHTML:
	    questionsScrapedSoFar = numberOfQuestions
	else:
	    soup = BeautifulSoup(newQuestionHTML.encode("utf-8"), 'html.parser')
	    questionsScrapedSoFarSoup = soup.find_all('a', class_= 'question_link')
	    questionsScrapedSoFar=0
	    for q in questionsScrapedSoFarSoup:
	        questionsScrapedSoFar+=1

	repeatCount = 0
	# Keep checking if there are new questions after scrolling down
	while (questionsScrapedSoFar < numberOfQuestionsToScrape):
		questionHTML = newQuestionHTML
		driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
		time.sleep(5)
		questionDiv = driver.find_element_by_class_name('layout_3col_center')
		newQuestionHTML = questionDiv.get_attribute("innerHTML")

		questionsScrapedSoFar += 5

	finalQuestions = questionDiv.get_attribute("innerHTML").encode("utf-8")

	# Get questions as strings
	soup = BeautifulSoup(finalQuestions, 'html.parser')
	questions = soup.find_all('a', class_= 'question_link')
	questionLinks = []
	for q in questions:
	    questionLinks.append(q['href'])

	# Need to add something in here in case quora messes up
	counter = 0
	for qLink in questionLinks:
		try:
			if counter >= 100:
				pass
			else:
				counter = counter + 1
				driver.get('https://www.quora.com'+qLink)

				# Get question text
				questionsText = driver.find_element_by_class_name('rendered_qtext').text    

				# Need to get number of answers
				try:
				    numberOfAnswersText = driver.find_element_by_class_name('answer_count').text.split(" ")[0].replace(',','').replace('+', '')
				except:
				    numberOfAnswersText = 1

				# Need to get number of views
				numberOfViewsText = driver.find_element_by_class_name('ViewsRow').text.split(" ")[0].replace(',','')

				try:
					driver.find_element_by_class_name('QuestionFollowerListModalLink')
					numberOfFollowersText = driver.find_element_by_class_name('QuestionFollowerListModalLink').text.split(" ")[0].replace(',','')
				except:
					numberOfFollowersText = 0

				tempdate = driver.find_element_by_class_name('AskedRow').text.split("Last Asked ")[1]

				try:
					dateask = datetime.strptime(tempdate, '%b %d, %Y')
				except:
					dateask = datetime.strptime(tempdate + ', 2018', '%b %d, %Y')

				Detail.objects.create(project_id = pk,title = questionsText, link = 'https://www.quora.com'+qLink, numviews = numberOfViewsText, numfollowers = numberOfFollowersText, numanswers = numberOfAnswersText, questiondate = dateask)
		except:
			pass

	Project.objects.filter(id=pk).update(status=1)
	# Close the window
	driver.close()