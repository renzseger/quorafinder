from django.contrib import admin

# Register your models here.
from .models import Project
from .models import Detail

class AuthorAdmin(admin.ModelAdmin):
	list_display = ('title', 'numviews', 'numfollowers', 'numanswers')

admin.site.register(Detail, AuthorAdmin)
admin.site.register(Project)

