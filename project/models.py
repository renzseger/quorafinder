from django.db import models

# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=200)
    phrase = models.CharField(max_length=200)
    status = models.BooleanField(default = 0)

    def save(self, *args, **kwargs):
        super(Project, self).save(*args, **kwargs)
        
        from project.tasks import save_details

        save_details.delay(self.pk, self.phrase)

    def __str__(self):
        return self.name

class Detail(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    title = models.CharField(max_length=254)
    link = models.CharField(max_length=254)
    numviews = models.CharField(max_length=200)
    numfollowers = models.CharField(max_length=200)
    numanswers = models.CharField(max_length=200)
    questiondate = models.DateField(auto_now=False, auto_now_add=False)

    def __str__(self):
        return self.link