var oTable;

$(document).ready(function(){
	$('.nav-item').on('click',function(e){
		$('.navbar-nav').find('li.nav-item.active').removeClass('active');
		$('#'+this.id).addClass('active');
	})
	
	if( document.getElementById("datatable2") ){
		oTable = $('#datatable2').dataTable({
			"iDisplayLength": 25,
			"bJQueryUI": true,
			"bStateSave": true
		}).yadcf([
		    {
		    	column_number : 4,
		    	filter_container_id: 'external_filter_container_2',
		    	filter_type: 'range_date'
		    }
		]
		,{	externally_triggered: true}
		);
	}
	
	$('#datatable').dataTable();

	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

});