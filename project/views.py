from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader

from .models import Project, Detail
from .forms import ProjectForm

def index(request):
    if request.user.is_authenticated:
        projects = Project.objects.all();
        template = loader.get_template('project/projects.html')
        context = {
            'projects': projects,
            'id' : 'projects',
        }
        return HttpResponse(template.render(context, request))
    else:
        return redirect('/login')

def detail(request, project_id):
    if request.user.is_authenticated:
        details = Detail.objects.filter(project_id=project_id);
        template = loader.get_template('project/details.html')
        context = {
            'details': details,
            'id' : 'projects',
        }
        return HttpResponse(template.render(context, request))
    else:
        return redirect('/login')

def deleteProject(request, project_id):
    Detail.objects.filter(project_id=project_id).delete();
    Project.objects.filter(id=project_id).delete();

    projects = Project.objects.all();
    template = loader.get_template('project/projects.html')
    context = {
        'projects': projects,
        'id' : 'projects',
    }
    return HttpResponse(template.render(context, request))

def downloadProject(request, project_id):
    Detail.objects.filter(project_id=project_id).delete();
    Project.objects.filter(id=project_id).delete();

    # projects = Project.objects.all();
    # template = loader.get_template('project/projects.html')
    # context = {
    #     'projects': projects,
    #     'id' : 'projects',
    # }
    # return HttpResponse(template.render(context, request))

    import io
    from django.http import FileResponse
    from reportlab.pdfgen import canvas

    def some_view(request):
        # Create a file-like buffer to receive PDF data.
        buffer = io.BytesIO()

        # Create the PDF object, using the buffer as its "file."
        p = canvas.Canvas(buffer)

        # Draw things on the PDF. Here's where the PDF generation happens.
        # See the ReportLab documentation for the full list of functionality.
        p.drawString(100, 100, "Hello world.")

        # Close the PDF object cleanly, and we're done.
        p.showPage()
        p.save()

        # FileResponse sets the Content-Disposition header so that browsers
        # present the option to save the file.
        return FileResponse(buffer, as_attachment=True, filename='hello.pdf')

def addProject(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = ProjectForm(request.POST)

            if form.is_valid():
                post = form.save(commit=False)
                post.save()


            projects = Project.objects.all();

            context = {
                'id' : 'projects',
                'projects': projects,
            }

            template = loader.get_template('project/projects.html')
            return HttpResponse(template.render(context, request))

        else:
            form = ProjectForm()

            project = [];
            template = loader.get_template('project/addProject.html')
            context = {
                'projects': project,
                'form': form,
                'id' : 'createproject',
            }
            return HttpResponse(template.render(context, request))
    else:
        return redirect('/login')