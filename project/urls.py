from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('project/add/', views.addProject, name='addProject'),
    path('project/edit/<int:project_id>', views.addProject, name='addProject'),
    path('project/delete/<int:project_id>', views.deleteProject, name='deleteProject'),
    path('project/pdf/<int:project_id>', views.downloadProject, name='downloadProject'),
    path('<int:project_id>/', views.detail, name='detail'),
]