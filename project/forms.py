from django import forms
from .models import Project, Detail

class ProjectForm(forms.ModelForm):

    class Meta:
        model = Project
        fields = ('name', 'phrase')