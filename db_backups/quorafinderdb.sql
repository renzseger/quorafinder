-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: qourafinderdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.2.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add user',3,'add_user'),(6,'Can change user',3,'change_user'),(7,'Can delete user',3,'delete_user'),(8,'Can view user',3,'view_user'),(9,'Can add group',4,'add_group'),(10,'Can change group',4,'change_group'),(11,'Can delete group',4,'delete_group'),(12,'Can view group',4,'view_group'),(13,'Can add permission',2,'add_permission'),(14,'Can change permission',2,'change_permission'),(15,'Can delete permission',2,'delete_permission'),(16,'Can view permission',2,'view_permission'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add question',7,'add_question'),(26,'Can change question',7,'change_question'),(27,'Can delete question',7,'delete_question'),(28,'Can view question',7,'view_question'),(29,'Can add choice',8,'add_choice'),(30,'Can change choice',8,'change_choice'),(31,'Can delete choice',8,'delete_choice'),(32,'Can view choice',8,'view_choice'),(41,'Can add project',11,'add_project'),(42,'Can change project',11,'change_project'),(43,'Can delete project',11,'delete_project'),(44,'Can view project',11,'view_project'),(45,'Can add detail',12,'add_detail'),(46,'Can change detail',12,'change_detail'),(47,'Can delete detail',12,'delete_detail'),(48,'Can view detail',12,'view_detail');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$150000$hsM3RY9Wg3WR$B9hW9DHJWxcEnVfWdJZztBdT/jqPPD+oF1svNz3Jbys=','2018-07-26 06:06:58.457040',1,'admin','','','admin@yopmail.com',1,1,'2018-07-25 04:53:46.793557'),(2,'pbkdf2_sha256$150000$q5gnPYm6Gz2l$h96yxZG/bGZXP0Dg2nLjKcv5LjLW40acmB9fiyrj3iw=','2018-07-26 06:03:40.586609',0,'default','default','defaul','default@yopmail.com',1,1,'2018-07-25 05:18:45.000000');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
INSERT INTO `auth_user_user_permissions` VALUES (18,2,41),(19,2,43),(20,2,44),(21,2,47),(17,2,48);
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2018-07-25 05:08:34.160691','2','sample',1,'[{\"added\": {}}]',7,1),(2,'2018-07-25 05:08:59.123128','3','asdad',1,'[{\"added\": {}}]',7,1),(3,'2018-07-25 05:09:08.019307','3','asdad',3,'',7,1),(4,'2018-07-25 05:17:15.891738','2','default',1,'[{\"added\": {}}]',3,1),(5,'2018-07-25 05:18:47.023675','2','default',2,'[{\"changed\": {\"fields\": [\"first_name\", \"last_name\", \"email\", \"user_permissions\", \"last_login\", \"date_joined\"]}}]',3,1),(6,'2018-07-25 05:20:01.119313','2','default',2,'[{\"changed\": {\"fields\": [\"password\"]}}]',3,1),(7,'2018-07-25 06:01:25.797343','1','LMTO',1,'[{\"added\": {}}]',9,1),(8,'2018-07-25 06:06:33.030043','2','default',2,'[{\"changed\": {\"fields\": [\"is_staff\"]}}]',3,1),(9,'2018-07-25 06:07:12.871100','2','default',2,'[{\"changed\": {\"fields\": [\"user_permissions\"]}}]',3,1),(10,'2018-07-25 06:40:34.319537','2','LMTO',1,'[{\"added\": {}}]',11,1),(11,'2018-07-25 06:42:15.062724','2','LMTO',3,'',11,1),(12,'2018-07-25 06:43:56.183605','4','LMTO',1,'[{\"added\": {}}]',11,1),(13,'2018-07-25 06:45:51.891937','4','LMTO',3,'',11,1),(14,'2018-07-25 06:49:39.454311','7','LMTO',1,'[{\"added\": {}}]',11,1),(15,'2018-07-25 06:49:52.807860','7','LMTO',3,'',11,1),(16,'2018-07-25 07:32:37.641194','10','LMTO',1,'[{\"added\": {}}]',11,1),(17,'2018-07-25 07:35:20.207153','10','LMTO',3,'',11,1),(18,'2018-07-25 07:35:49.477941','12','LMTO',1,'[{\"added\": {}}]',11,1),(19,'2018-07-25 07:36:10.073587','12','LMTO',3,'',11,1),(20,'2018-07-25 07:40:45.352627','16','LMTO',1,'[{\"added\": {}}]',11,1),(21,'2018-07-25 07:43:04.884185','16','LMTO',3,'',11,1),(22,'2018-07-25 07:43:10.304794','17','LMTO',1,'[{\"added\": {}}]',11,1),(23,'2018-07-25 07:48:23.783673','17','LMTO',3,'',11,1),(24,'2018-07-25 07:49:22.671644','18','LMTO',1,'[{\"added\": {}}]',11,1),(25,'2018-07-25 07:50:24.069441','18','LMTO',3,'',11,1),(26,'2018-07-25 07:51:07.096961','19','LMTO',1,'[{\"added\": {}}]',11,1),(27,'2018-07-25 07:51:31.335360','19','LMTO',3,'',11,1),(28,'2018-07-25 07:52:29.489721','20','LMTO',1,'[{\"added\": {}}]',11,1),(29,'2018-07-25 07:53:12.170542','20','LMTO',3,'',11,1),(30,'2018-07-25 07:53:17.352203','21','LMTO',1,'[{\"added\": {}}]',11,1),(31,'2018-07-25 07:54:58.283793','21','LMTO',3,'',11,1),(32,'2018-07-25 07:55:44.486661','22','LMTO',1,'[{\"added\": {}}]',11,1),(33,'2018-07-25 07:57:18.934478','22','LMTO',3,'',11,1),(34,'2018-07-25 07:59:32.167789','23','LMTO',1,'[{\"added\": {}}]',11,1),(35,'2018-07-25 08:00:54.175957','23','LMTO',3,'',11,1),(36,'2018-07-25 08:01:34.906477','24','LMTO',1,'[{\"added\": {}}]',11,1),(37,'2018-07-25 08:02:09.372409','24','LMTO',3,'',11,1),(38,'2018-07-25 08:02:56.391212','25','LMTO',1,'[{\"added\": {}}]',11,1),(39,'2018-07-26 02:27:47.625227','25','LMTO',3,'',11,1),(40,'2018-07-26 02:53:26.409094','30','LMTO',1,'[{\"added\": {}}]',11,1),(41,'2018-07-26 05:14:41.260587','30','LMTO',3,'',11,1),(42,'2018-07-26 05:18:19.540681','31','LMTO',1,'[{\"added\": {}}]',11,1),(43,'2018-07-26 05:46:55.760281','31','LMTO',3,'',11,1),(44,'2018-07-26 05:51:06.125017','32','LMTO',1,'[{\"added\": {}}]',11,1),(45,'2018-07-26 06:00:22.524966','2','default',2,'[{\"changed\": {\"fields\": [\"user_permissions\"]}}]',3,1),(46,'2018-07-26 06:01:51.261672','2','default',2,'[{\"changed\": {\"fields\": [\"user_permissions\"]}}]',3,1),(47,'2018-07-26 06:03:25.211447','2','default',2,'[{\"changed\": {\"fields\": [\"user_permissions\"]}}]',3,1),(48,'2018-07-26 06:05:09.647791','33','assetname',1,'[{\"added\": {}}]',11,2);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(4,'auth','group'),(2,'auth','permission'),(3,'auth','user'),(5,'contenttypes','contenttype'),(8,'polls','choice'),(7,'polls','question'),(12,'project','detail'),(10,'project','details'),(11,'project','project'),(9,'project','projects'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-07-25 03:18:53.357534'),(2,'auth','0001_initial','2018-07-25 03:18:55.606721'),(3,'admin','0001_initial','2018-07-25 03:18:56.100092'),(4,'admin','0002_logentry_remove_auto_add','2018-07-25 03:18:56.111361'),(5,'admin','0003_logentry_add_action_flag_choices','2018-07-25 03:18:56.122967'),(6,'contenttypes','0002_remove_content_type_name','2018-07-25 03:18:56.166775'),(7,'auth','0002_alter_permission_name_max_length','2018-07-25 03:18:56.302378'),(8,'auth','0003_alter_user_email_max_length','2018-07-25 03:18:56.472019'),(9,'auth','0004_alter_user_username_opts','2018-07-25 03:18:56.482955'),(10,'auth','0005_alter_user_last_login_null','2018-07-25 03:18:56.506078'),(11,'auth','0006_require_contenttypes_0002','2018-07-25 03:18:56.509038'),(12,'auth','0007_alter_validators_add_error_messages','2018-07-25 03:18:56.520418'),(13,'auth','0008_alter_user_username_max_length','2018-07-25 03:18:56.638383'),(14,'auth','0009_alter_user_last_name_max_length','2018-07-25 03:18:56.816231'),(15,'sessions','0001_initial','2018-07-25 03:18:56.961913'),(16,'polls','0001_initial','2018-07-25 03:41:43.823434'),(19,'project','0001_initial','2018-07-25 06:35:55.168445');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('pm0mxkyy0q264lq7ag6muio841s0ywql','YThlMTZiNjI1ZTVjMzIyZjYzNDAwNjQwN2RjOGM5ZjI4ZmZhNmQ3OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjA1NDU3NjFlODJmOTE1NTVjZDAwYTg1MTEzY2ZmZGQyYWM0NmI5NiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-08-09 06:06:58.459556');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_detail`
--

DROP TABLE IF EXISTS `project_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(254) NOT NULL,
  `link` varchar(254) NOT NULL,
  `numviews` varchar(200) NOT NULL,
  `numfollowers` varchar(200) NOT NULL,
  `numanswers` varchar(200) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_detail_project_id_309eac6f_fk_project_project_id` (`project_id`),
  CONSTRAINT `project_detail_project_id_309eac6f_fk_project_project_id` FOREIGN KEY (`project_id`) REFERENCES `project_project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_detail`
--

LOCK TABLES `project_detail` WRITE;
/*!40000 ALTER TABLE `project_detail` DISABLE KEYS */;
INSERT INTO `project_detail` VALUES (107,'What should I do with the $100,000 lump sum I\'m about to receive from life insurance? I\'m 67-years-old.','https://www.quora.com/What-should-I-do-with-the-100-000-lump-sum-Im-about-to-receive-from-life-insurance-Im-67-years-old','2208','1','13',32),(108,'At what age should I buy life insurance?','https://www.quora.com/At-what-age-should-I-buy-life-insurance','16054','1','54',32),(109,'What is the advantage of getting life insurance when you\'re in your 20s?','https://www.quora.com/What-is-the-advantage-of-getting-life-insurance-when-youre-in-your-20s','720','1','10',32),(110,'Is it possible to buy health insurance outside of the open enrollment period (Not counting \"special circumstances\")? Is private health insurance even an option anymore?','https://www.quora.com/Is-it-possible-to-buy-health-insurance-outside-of-the-open-enrollment-period-Not-counting-special-circumstances-Is-private-health-insurance-even-an-option-anymore','267','1','6',32),(111,'What are the pros and cons of term life insurance?','https://www.quora.com/What-are-the-pros-and-cons-of-term-life-insurance','7861','1','22',32),(112,'Does my beneficiary need to know that I have life insurance from my company in order for them to claim the benefits if I die? Or will my employer or insurance company automatically contact them?','https://www.quora.com/Does-my-beneficiary-need-to-know-that-I-have-life-insurance-from-my-company-in-order-for-them-to-claim-the-benefits-if-I-die-Or-will-my-employer-or-insurance-company-automatically-contact-them','421','1','15',32),(113,'What is an accidental death rider?','https://www.quora.com/What-is-an-accidental-death-rider','973','1','1',32),(114,'Why do I need life insurance?','https://www.quora.com/Why-do-I-need-life-insurance','5092','1','46',32),(115,'How do insurance companies decide to pay for drugs that don\'t have a clear clinical benefit?','https://www.quora.com/How-do-insurance-companies-decide-to-pay-for-drugs-that-dont-have-a-clear-clinical-benefit','620','1','3',32),(116,'What are the indexed universal life (IUL) products with no cap?','https://www.quora.com/What-are-the-indexed-universal-life-IUL-products-with-no-cap','402','1','5',32),(117,'What should you never do when buying an insurance policy?','https://www.quora.com/What-should-you-never-do-when-buying-an-insurance-policy','446','1','10',32),(118,'What can happen if a life insurance agent commits fraud on someone?','https://www.quora.com/What-can-happen-if-a-life-insurance-agent-commits-fraud-on-someone','581','1','7',32),(119,'How do I become a believer of life insurance?','https://www.quora.com/How-do-I-become-a-believer-of-life-insurance','604','1','8',32),(120,'How good is the ULIP Max Life Platinum Wealth plan for a 33-year-old adult?','https://www.quora.com/How-good-is-the-ULIP-Max-Life-Platinum-Wealth-plan-for-a-33-year-old-adult','758','1','2',32),(121,'Why is it considered illegal to not have health insurance?','https://www.quora.com/Why-is-it-considered-illegal-to-not-have-health-insurance','4909','1','6',32),(122,'Do financial advisers get a commission from selling whole/cash-value/permanent life insurance? For reasons I won\'t get into (I\'ve done my research), I don\'t believe it is a good investment, but my adviser is uncomfortably resistant.','https://www.quora.com/Do-financial-advisers-get-a-commission-from-selling-whole-cash-value-permanent-life-insurance-For-reasons-I-wont-get-into-Ive-done-my-research-I-dont-believe-it-is-a-good-investment-but-my-adviser-is-uncomfortably','393','1','9',32),(123,'How can a car insurance company admit they are liable for an accident and then recant that statement? Is this legal?','https://www.quora.com/How-can-a-car-insurance-company-admit-they-are-liable-for-an-accident-and-then-recant-that-statement-Is-this-legal','306','1','7',32),(124,'How does profession play an important role in a customer purchasing an accident insurance policy?','https://www.quora.com/How-does-profession-play-an-important-role-in-a-customer-purchasing-an-accident-insurance-policy','360','1','2',32),(125,'What is the principal of life insurance?','https://www.quora.com/What-is-the-principal-of-life-insurance','749','1','4',32),(126,'Is Oscar an insurance carrier or a broker?','https://www.quora.com/Is-Oscar-an-insurance-carrier-or-a-broker','874','1','1',32),(127,'Does my beneficiary need to know that I have life insurance from my company in order for them to claim the benefits if I die? Or will my employer or insurance company automatically contact them?','https://www.quora.com/Does-my-beneficiary-need-to-know-that-I-have-life-insurance-from-my-company-in-order-for-them-to-claim-the-benefits-if-I-die-Or-will-my-employer-or-insurance-company-automatically-contact-them','421','1','15',33),(128,'Is it possible to buy health insurance outside of the open enrollment period (Not counting \"special circumstances\")? Is private health insurance even an option anymore?','https://www.quora.com/Is-it-possible-to-buy-health-insurance-outside-of-the-open-enrollment-period-Not-counting-special-circumstances-Is-private-health-insurance-even-an-option-anymore','267','1','6',33),(129,'Why do I need life insurance?','https://www.quora.com/Why-do-I-need-life-insurance','5094','1','46',33),(130,'What are the pros and cons of term life insurance?','https://www.quora.com/What-are-the-pros-and-cons-of-term-life-insurance','7861','1','22',33),(131,'What is the principal of life insurance?','https://www.quora.com/What-is-the-principal-of-life-insurance','749','1','4',33),(132,'What services do business travellers spend their money on?','https://www.quora.com/What-services-do-business-travellers-spend-their-money-on','4350','1','2',33),(133,'What is the slogan for the insurance company Progressive?','https://www.quora.com/What-is-the-slogan-for-the-insurance-company-Progressive','11566','1','5',33),(134,'Is it normal for a 20 year old to buy life insurance?','https://www.quora.com/Is-it-normal-for-a-20-year-old-to-buy-life-insurance','1299','1','7',33),(135,'What are all the plans Geoblue insurance having and the countries they are providing insurance to?','https://www.quora.com/What-are-all-the-plans-Geoblue-insurance-having-and-the-countries-they-are-providing-insurance-to','211','1','1',33),(136,'How do I become a believer of life insurance?','https://www.quora.com/How-do-I-become-a-believer-of-life-insurance','604','1','8',33),(137,'What minimum credit rating should I accept when selecting a life insurance company?','https://www.quora.com/What-minimum-credit-rating-should-I-accept-when-selecting-a-life-insurance-company','797','1','4',33),(138,'Why is our healthcare so expensive and yet we are so chronically sick?','https://www.quora.com/Why-is-our-healthcare-so-expensive-and-yet-we-are-so-chronically-sick','25273','1','14',33),(139,'What is the relationship between insurance brokers and insurance companies (underwriters)? Why do brokers exist as a separate actor in the value chain?','https://www.quora.com/What-is-the-relationship-between-insurance-brokers-and-insurance-companies-underwriters-Why-do-brokers-exist-as-a-separate-actor-in-the-value-chain','1607','1','4',33),(140,'How do I write a letter of request to a life insurance company to reinstate a lapsed policy, or refund the whole amount?','https://www.quora.com/How-do-I-write-a-letter-of-request-to-a-life-insurance-company-to-reinstate-a-lapsed-policy-or-refund-the-whole-amount','1038','1','5',33),(141,'I am 21 and working, do I get life insurance this early? I am thinking of getting it 2-3 years from now since I want to enjoy at the moment.','https://www.quora.com/I-am-21-and-working-do-I-get-life-insurance-this-early-I-am-thinking-of-getting-it-2-3-years-from-now-since-I-want-to-enjoy-at-the-moment','1880','1','8',33),(142,'What can happen if a life insurance agent commits fraud on someone?','https://www.quora.com/What-can-happen-if-a-life-insurance-agent-commits-fraud-on-someone','581','1','7',33),(143,'What is an ideal bonus policy for a marketing department?','https://www.quora.com/What-is-an-ideal-bonus-policy-for-a-marketing-department','235','1','1',33),(144,'Is the Airbnb $1 million host guarantee a marketing gimmick? Has anyone successfully submitted a claim and received money?','https://www.quora.com/Is-the-Airbnb-1-million-host-guarantee-a-marketing-gimmick-Has-anyone-successfully-submitted-a-claim-and-received-money','21570','1','10',33),(145,'Why is Whole Life insurance a good idea?','https://www.quora.com/Why-is-Whole-Life-insurance-a-good-idea','2408','1','9',33);
/*!40000 ALTER TABLE `project_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_project`
--

DROP TABLE IF EXISTS `project_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `phrase` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_project`
--

LOCK TABLES `project_project` WRITE;
/*!40000 ALTER TABLE `project_project` DISABLE KEYS */;
INSERT INTO `project_project` VALUES (32,'LMTO','Life Insurance'),(33,'assetname','Life Insurance');
/*!40000 ALTER TABLE `project_project` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-26 15:02:26
